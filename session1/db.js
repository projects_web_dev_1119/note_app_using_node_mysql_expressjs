const mysql = require('mysql')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'manager',
  database: 'notes_db',
  port: 3306,

  // connection limit at the time
  connectionLimit: 20,
})

module.exports = {
  pool,
}
