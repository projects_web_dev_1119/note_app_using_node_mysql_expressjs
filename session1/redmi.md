# note application

```bash
> yarn add express mysql crypto-js jsonwebtoken cors

> node server.js 

```

## technology

- server : express
- platform : nodejs
- database : mysql
- external libraries uses : mysql,crypto-js, jsonwebtoken,cors

## Database

```sql
CREATE DATABASE notes_db;

USE notes_db;

CREATE TABLE user (id INT PRIMARY KEY auto_increment,firstName varchar(20),lastName varchar(20),email varchar(30),password varchar(100),createdTimestamp timestamp DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE note (id INT PRIMARY KEY auto_increment,title varchar(30),content varchar(1000),userId INT,createdTimestamp timestamp DEFAULT CURRENT_TIMESTAMP );

```

