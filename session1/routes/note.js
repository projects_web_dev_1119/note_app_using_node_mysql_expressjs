const express = require('express')
const db = require('../db')
const utils = require('../Utils')

const router = express.Router()

// add the routes
// get all notes
router.get('/', (request, response) => {
  //   const { userId } = request.params
  const statement = `SELECT id,title,content,date FROM note WHERE userId = ?`
  // fire the query
  db.pool.query(statement, [request.userId], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
// add the note
router.post('/', (request, response) => {
  const { title, content } = request.body
  const statement = `INSERT INTO note (title,content,userId) VALUES(?,?,?)`
  db.pool.query(statement, [title, content, request.userId], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
// update the note
router.put('/:noteId', (request, response) => {
  const { noteId } = request.params
  const { title, content } = request.body
  const statement = `UPDATE note SET title = ? , content = ? WHERE id = ? AND userId = ?`
  // fire the query
  db.pool.query(
    statement,
    [title, content, noteId, request.userId],
    (error, note) => {
      response.send(utils.createResult(error, note))
    }
  )
})

// delete the note
router.delete('/:noteId', (request, response) => {
  const { noteId } = request.params
  const statement = `DELETE FROM note WHERE id = ? AND userId = ?`
  // fire the query
  db.pool.query(statement, [noteId, request.userId], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
module.exports = router
