const express = require('express')
const cryptoJs = require('crypto-js')
const db = require('../db')
const utils = require('../Utils')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()
// -- add the routes

// signup user
router.post('/signup', (request, response) => {
  const { firstName, lastName, email, password } = request.body
  // encrypt the password to protect it
  const encryptedPassword = String(cryptoJs.MD5(password))

  // query statement
  // use ? to avoid the sql injection as the placeholder
  const statement = `INSERT INTO user (firstName,lastName,email,password)
  VALUES (?,?,?,?)`
  // fire the query
  db.pool.query(
    statement,
    [firstName, lastName, email, encryptedPassword],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

// signin user
router.get('/signin', (request, response) => {
  const { email, password } = request.body
  // statement
  const statement = `SELECT id,firstName,lastName,email FROM user WHERE
  email = ? AND password = ?`
  const encryptedPassword = String(cryptoJs.MD5(password))
  // fire the query
  db.pool.query(statement, [email, encryptedPassword], (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else if (users.length == 0) {
      result['status'] = 'success'
      result['error'] = 'user does not exist'
    } else {
      // authentication is successful
      const user = users[0]
      result['status'] = 'success'
      //if sucess create the token
      // used to verify the user usign the signature
      const token = jwt.sign(
        // payload : this will send to all the api's
        {
          userId: user['id'],
        },
        // secret : used to generate the token
        config.secret
      )
      // lets don't send the userId from signin
      // so user don't find userId anywhere
      result['data'] = {
        firstName: user['firstName'],
        lastName: user['lastName'],
        email: user['email'],
        token,
      }
    }
    response.send(result)
  })
})

module.exports = router
