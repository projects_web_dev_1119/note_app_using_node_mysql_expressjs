const express = require('express')
const jwt = require('jsonwebtoken')
const cors = require('cors')
const utils = require('./Utils')
const config = require('./config')

const app = express()

// enable cors
// for client to make the api calls to the server
app.use(cors())
// use the  json-body parser
app.use(express.json())
// add the middleware to extract the token
app.use((request, response, next) => {
  console.log(request.url)
  // check if the token is required or not
  // token is not needed for following apis
  // -signin,signup,reset-password,forget-password
  if (request.url == '/user/signup' || request.url == '/user/signin') {
    // simply call the next function
    // skip the checking of the token
    next()
  } else {
    // get the token from header
    const token = request.headers['token']

    // check if the token is not empty
    if (!token || token.length == 0) {
      // token is empty undefined or null
      response.send(utils.createResult('token is missing'))
    } else {
      // if token is not empty
      // decode the token
      try {
        const payload = jwt.verify(token, config.secret)

        // extract the userId from the token recives
        // add the userId to the request object
        request.userId = payload.userId

        // call the real function
        // when the next function is called, it will recives the same request
        // the object which has the userId in it
        next()
      } catch {
        // invalid token temperred token
        response.send(utils.createResult('invalid token'))
      }
    }
  }
})
// import the routes
const userRouter = require('./routes/user')
const noteRouter = require('./routes/note')

// use the routes
app.use('/user', userRouter)
app.use('/note', noteRouter)
app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})
